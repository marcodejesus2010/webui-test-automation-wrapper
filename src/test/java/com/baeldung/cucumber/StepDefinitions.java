package com.baeldung.cucumber;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import java.nio.DoubleBuffer;

public class StepDefinitions {

    Account account;

    @Given("account balance is {double}")
    public void givenAccountBalance(Double initialBalance){
        account = new Account(initialBalance);
    }

    @When("the account is credited with {double}")
    public void the_account_is_credited_with(Double double1) {
        // Write code here that turns the phrase above into concrete actions
       // throw new io.cucumber.java.PendingException();
    }

    @Then("account should have a balance of {double}")
    public void account_should_have_a_balance_of(Double double1) {
        // Write code here that turns the phrase above into concrete actions
        //throw new io.cucumber.java.PendingException();
    }

}
