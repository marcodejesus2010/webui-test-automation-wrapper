import core.browser.ChromeBrowserDriverImpl;
import core.browser.FirefoxBrowserDriverImpl;
import core.browser.WebDriverManager;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class SimpleTest {
    @Test
    void one(){
        WebDriver driver = WebDriverManager.getDriverInstance(new FirefoxBrowserDriverImpl());
        driver.get("https://www.saucedemo.com/");

        WebElement usernameTextBox = driver.findElement(By.id("user-name"));
        usernameTextBox.sendKeys("standard_user");

        WebElement passwordTextBox = driver.findElement(By.id("password"));
        passwordTextBox.sendKeys("secret_sauce");

        WebElement loginButton = driver.findElement(By.id("login-button"));
        loginButton.click();

        WebElement appLogo = driver.findElement(By.className("app_logo"));

        assertThat(appLogo.isDisplayed(), is(true));
    }
}
