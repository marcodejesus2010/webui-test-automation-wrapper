package core.browser;

import org.openqa.selenium.WebDriver;

public class WebDriverManager {
    public static WebDriver getDriverInstance(BrowserDriver browserDriver){
        return browserDriver.getBrowserDriverInstance();
    }
}
