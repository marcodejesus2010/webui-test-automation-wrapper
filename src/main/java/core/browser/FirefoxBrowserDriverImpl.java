package core.browser;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class FirefoxBrowserDriverImpl implements BrowserDriver{
    private final static String CHROME_DRIVER = "webdriver.gecko.driver";
    private final static String CHROME_DRIVER_PATH = "src/main/resources/drivers/geckodriver";

    public FirefoxBrowserDriverImpl(){
        System.setProperty(CHROME_DRIVER, CHROME_DRIVER_PATH);
    }

    @Override
    public WebDriver getBrowserDriverInstance() {
        return new FirefoxDriver();
    }
}
