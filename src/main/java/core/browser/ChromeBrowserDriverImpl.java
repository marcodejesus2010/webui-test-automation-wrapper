package core.browser;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class ChromeBrowserDriverImpl implements BrowserDriver{
    private final static String CHROME_DRIVER = "webdriver.chrome.driver";
    private final static String CHROME_DRIVER_PATH = "src/main/resources/drivers/chromedriver";

    public ChromeBrowserDriverImpl(){
        System.setProperty(CHROME_DRIVER, CHROME_DRIVER_PATH);
    }

    @Override
    public WebDriver getBrowserDriverInstance() {
        return new ChromeDriver();
    }
}
