package core.browser;

import org.openqa.selenium.WebDriver;

public interface BrowserDriver {
    WebDriver getBrowserDriverInstance();
}
